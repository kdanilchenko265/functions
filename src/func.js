const getSum = (str1, str2) => {
  let check=(/^[0-9.,]+$/.test(str1)&&/^[0-9.,]+$/.test(str2))?true:false;
  let res;
  if(check||str1==''&&typeof str1=='string'||str2==''&&typeof str2=='string'){
    if(str1==''){
      str1=0;
      str1=parseInt(str1);
    }else{str1=parseInt(str1);}
    if(str2==''){
      str2=0;
      str2=parseInt(str2);
    }else{str2=parseInt(str2);}
    res=str1+str2;
    return res.toString();
  }else{
    return false;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPost = 0,CountComment = 0;
  for(const post of listOfPosts){
    if(post.author == authorName){
      countPost++;
    }
    if(post.hasOwnProperty("comments")){
    for(let i =0;i< post["comments"].length;i++){
      if(post["comments"][i].author == authorName){
        CountComment++;
      }}
    }
  }
  return 'Post:'+countPost+',comments:'+CountComment;
};

const tickets=(people)=> {
  let moneyOfclerk=0;
  let ticketCosts=25;
  let result;
 if(people[0]==ticketCosts){
  moneyOfclerk+=ticketCosts;
  for(let i=1;i<=people.length-1;i++)
  {
    result=(people[i]-ticketCosts<=moneyOfclerk)?'YES' :'NO';
    if(result){moneyOfclerk+=ticketCosts;}
  }
  return result;
 }else{return 'NO';}
 
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
